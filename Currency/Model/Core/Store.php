<?php
/**
 * Avestique Developer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@avestique.ru so we can send you a copy immediately.
 *
 * @category    Avestique
 * @package     Avestique_Currency
 * @copyright   Copyright (c) 2013 Avestique Developer. (http://www.avestique.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @file Store.php
 */

class Avestique_Currency_Model_Core_Store extends Mage_Core_Model_Store
{
    /**
     * Round price
     *
     * @param mixed $price
     * @return double
     */
    public function roundPrice($price)
    {
        $roundValue = (int) Mage::getSingleton('adminhtml/config_data')->getConfigDataValue(Avestique_Currency_Helper_Data::XML_PATH_STORE_PRICE_ROUND);

        $roundUpValue = (int) Mage::getSingleton('adminhtml/config_data')->getConfigDataValue(Avestique_Currency_Helper_Data::XML_PATH_STORE_PRICE_ROUND_UP);

        if ($roundUpValue)
            return ceil($price);

        return round($price, $roundValue ? $roundValue : 0);
    }

    /**
     * Format price with currency filter (taking rate into consideration)
     *
     * @param   double $price
     * @param   bool $includeContainer
     * @return  string
     */
    public function formatPrice($price, $includeContainer = true, $options = array())
    {
        $price = $this->roundPrice($price);

        $options = Mage::helper('av_currency')->applyOptions($options);

        if ($this->getCurrentCurrency()) {
            return $this->getCurrentCurrency()->format($price, $options, $includeContainer);
        }

        return $price;
    }
}