<?php
/**
 * Avestique Developer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@avestique.ru so we can send you a copy immediately.
 *
 * @category    Avestique
 * @package     Avestique_Currency
 * @copyright   Copyright (c) 2013 Avestique Developer. (http://www.avestique.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @file Avestique_Currency_Model_System_Config_Source.php
 *
 * @info http://framework.zend.com/manual/1.5/en/zend.locale.parsing.html#zend.locale.appendix.numberscripts.supported
 */
class Avestique_Currency_Model_Adminhtml_System_Config_Source_Script
{
    public function toOptionArray()
    {
        return array(
            array('value' => '',    'label' => ""),
            array('value' =>'Arab', 'label' => Mage::helper('av_currency')->__("Arabic")),
            array('value' =>'Bali', 'label' => Mage::helper('av_currency')->__("Balinese")),
            array('value' =>'Beng', 'label' => Mage::helper('av_currency')->__("Bengali")),
            array('value' =>'Deva', 'label' => Mage::Helper('av_currency')->__('Devanagari')),
            array('value' =>'Gujr', 'label' => Mage::Helper('av_currency')->__('Gujarati')),
            array('value' =>'Guru', 'label' => Mage::Helper('av_currency')->__('Gurmukhi')),
            array('value' =>'Knda', 'label' => Mage::Helper('av_currency')->__('Kannada')),
            array('value' =>'Khmr', 'label' => Mage::Helper('av_currency')->__('Khmer')),
            array('value' =>'Laoo', 'label' => Mage::Helper('av_currency')->__('Lao')),
            array('value' =>'Limb', 'label' => Mage::Helper('av_currency')->__('Limbu')),
            array('value' =>'Mlym', 'label' => Mage::Helper('av_currency')->__('Malayalam')),
            array('value' =>'Mong', 'label' => Mage::Helper('av_currency')->__('Mongolian')),
            array('value' =>'Mymr', 'label' => Mage::Helper('av_currency')->__('Myanmar')),
            array('value' =>'Talu', 'label' => Mage::Helper('av_currency')->__('New_Tai_Lue')),
            array('value' =>'Nkoo', 'label' => Mage::Helper('av_currency')->__('Nko')),
            array('value' =>'Orya', 'label' => Mage::Helper('av_currency')->__('Oriya')),
            array('value' =>'Taml', 'label' => Mage::Helper('av_currency')->__('Tamil')),
            array('value' =>'Telu', 'label' => Mage::Helper('av_currency')->__('Telugu')),
            array('value' =>'Tale', 'label' => Mage::Helper('av_currency')->__('Thai')),
            array('value' =>'Tibt', 'label' => Mage::helper('av_currency')->__("Tibetan"))
        );
    }
}