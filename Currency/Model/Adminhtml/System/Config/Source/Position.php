<?php
/**
 * Avestique Developer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@avestique.ru so we can send you a copy immediately.
 *
 * @category    Avestique
 * @package     Avestique_Currency
 * @copyright   Copyright (c) 2013 Avestique Developer. (http://www.avestique.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @file Position.php
 */

class Avestique_Currency_Model_Adminhtml_System_Config_Source_Position
{
    public function toOptionArray()
    {
        return array(
            //array('value'=>Zend_Currency::STANDARD, 'label'=>Mage::helper('av_currency')->__("Standard")),
            array('value'=>Zend_Currency::RIGHT,    'label'=>Mage::helper('av_currency')->__("Right")),
            array('value'=>Zend_Currency::LEFT,     'label'=>Mage::helper('av_currency')->__("Left"))
        );
    }
}
