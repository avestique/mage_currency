<?php
/**
 * Avestique Developer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@avestique.ru so we can send you a copy immediately.
 *
 * @category    Avestique
 * @package     Avestique_Currency
 * @copyright   Copyright (c) 2013 Avestique Developer. (http://www.avestique.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @file Display.php
 */

class Avestique_Currency_Model_Adminhtml_System_Config_Source_Display
{
    public function toOptionArray()
    {
        return array(
            array('value'=>Zend_Currency::NO_SYMBOL,     'label'=>Mage::helper('av_currency')->__("No symbol")),
            array('value'=>Zend_Currency::USE_SYMBOL,    'label'=>Mage::helper('av_currency')->__("Use symbol")),
            array('value'=>Zend_Currency::USE_SHORTNAME, 'label'=>Mage::helper('av_currency')->__("Use short name")),
            array('value'=>Zend_Currency::USE_NAME,      'label'=>Mage::helper('av_currency')->__("Use name")),
        );
    }
}
