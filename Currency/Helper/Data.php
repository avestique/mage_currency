<?php
/**
 * Avestique Developer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@avestique.ru so we can send you a copy immediately.
 *
 * @category    Avestique
 * @package     Avestique_Core
 * @copyright   Copyright (c) 2013 Avestique Developer. (http://www.avestique.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @file Data.php
 */

class Avestique_Currency_Helper_Data extends Mage_Core_Helper_Abstract
{
    const XML_PATH_STORE_PRICE_ROUND          = 'currency/options/av_round';
    const XML_PATH_STORE_PRICE_ROUND_UP       = 'currency/options/av_round_up';
    const XML_PATH_STORE_PRICE_PRECISION      = 'currency/options/av_precision';
    const XML_PATH_STORE_PRICE_SIGN_POSITION  = 'currency/options/av_position';
    const XML_PATH_STORE_PRICE_DISPLAY        = 'currency/options/av_display';
    const XML_PATH_STORE_PRICE_SCRIPT         = 'currency/options/av_script';
    const XML_PATH_STORE_PRICE_FORMAT         = 'currency/options/av_format';
    const XML_PATH_STORE_PRICE_LOCALE         = 'currency/options/av_locale';
    const XML_PATH_STORE_CURRENCY_DEFAULT     = 'currency/options/default';

    //const XML_PATH_STORE_PRICE_NAME           = 'currency/options/av_';
    //const XML_PATH_STORE_PRICE_SYMBOL         = 'currency/options/av_';

    public function applyOptions($currentOptions = array())
    {
        $currency_options = $currentOptions;

        $precisionValue = (int) Mage::getSingleton('adminhtml/config_data')->getConfigDataValue(self::XML_PATH_STORE_PRICE_PRECISION);

        $currency_options['precision'] = $precisionValue;

        $formatValue = (string) Mage::getSingleton('adminhtml/config_data')->getConfigDataValue(self::XML_PATH_STORE_PRICE_FORMAT);

        if ($formatValue)
            $currency_options['format'] = $formatValue;

        if (!isset($currency_options['position']))
        {
            $positionValue = (int) Mage::getSingleton('adminhtml/config_data')->getConfigDataValue(self::XML_PATH_STORE_PRICE_SIGN_POSITION);

            $currency_options['position'] = $positionValue;
        }

        /*$localeValue = (string) Mage::getSingleton('adminhtml/config_data')->getConfigDataValue(self::XML_PATH_STORE_PRICE_LOCALE);

        if ($localeValue)
            $currency_options['locale'] = $localeValue;*/

        $defaultCurrency = Mage::getModel('directory/currency')->getConfigDefaultCurrencies();
        $defaultCurrencyCode = $defaultCurrency ? current($defaultCurrency) : NULL;

        switch( (int) Mage::getSingleton('adminhtml/config_data')->getConfigDataValue(self::XML_PATH_STORE_PRICE_DISPLAY) )
        {
            case Zend_Currency::NO_SYMBOL:
                $currency_options['display'] = Zend_Currency::NO_SYMBOL;
                $currency_options['symbol'] = NULL;
                $currency_options['currency'] = NULL;
                $currency_options['name'] = NULL;
                break;
            case Zend_Currency::USE_SHORTNAME:
                $currency_options['display'] = Zend_Currency::USE_SHORTNAME;
                $currency_options['currency'] = $defaultCurrencyCode;
                $currency_options['symbol'] = NULL;
                $currency_options['name'] = NULL;
                break;
            case Zend_Currency::USE_NAME:
                $symbolData = Mage::getModel('currencySymbol/system_currencysymbol')->getCurrencySymbolsData();
                $currency_options['display'] = Zend_Currency::USE_NAME;
                $currency_options['name'] = isset($symbolData[$defaultCurrencyCode]) ? $symbolData[$defaultCurrencyCode]['displayName'] : '';
                $currency_options['symbol'] = NULL;
                unset($currency_options['currency']);
                break;
            case Zend_Currency::USE_SYMBOL:
            default:
                break;
        }

        return $currency_options;
    }
}