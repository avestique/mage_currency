<?php
/**
 * Avestique Developer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@avestique.ru so we can send you a copy immediately.
 *
 * @category    Avestique
 * @package     Avestique_Currency
 * @copyright   Copyright (c) 2013 Avestique Developer. (http://www.avestique.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @file Position.php
 */

class Avestique_Currency_Model_Adminhtml_System_Config_Source_Precision
{
    public function toOptionArray()
    {
        return array(
            array('value'=>0, 'label'=>Mage::helper('av_currency')->__("0")),
            array('value'=>1, 'label'=>Mage::helper('av_currency')->__("1")),
            array('value'=>2, 'label'=>Mage::helper('av_currency')->__("2")),
            array('value'=>3, 'label'=>Mage::helper('av_currency')->__("3")),
            array('value'=>4, 'label'=>Mage::helper('av_currency')->__("4")),
            array('value'=>5, 'label'=>Mage::helper('av_currency')->__("5")),
            array('value'=>6, 'label'=>Mage::helper('av_currency')->__("6")),
            array('value'=>7, 'label'=>Mage::helper('av_currency')->__("7")),
            array('value'=>8, 'label'=>Mage::helper('av_currency')->__("8"))
        );
    }
}
